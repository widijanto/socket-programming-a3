import socket
import datetime as dt
from datetime import datetime

HEADER = 80
FORMAT = 'utf-8'

PORT = 8080
SERVER = ''
ADDR = (SERVER, PORT)


def isPalindrome(string): 
    return string == string[::-1]


def minPalPartion(string, start, end): 
    if start >= end or isPalindrome(string[start:end + 1]): 
        return 0
    ans = float('inf') 
    for huruf in range(start, end): 
        count = ( 
            1 + minPalPartion(string, start, huruf) 
            + minPalPartion(string, huruf + 1, end) 
        ) 
        ans = min(ans, count) 
    return ans 


def find_palpart(conn, server):
    # Receive string data dari client 
    full_string = ''
    first_string = True
    accepting = True
    while accepting:
        string = conn.recv(16).decode(FORMAT)
        if first_string:
            string_len = int(string[:HEADER])
            first_string = False

        full_string += string

        if len(full_string)-HEADER == string_len:
            accepting = False
    full_string = full_string[HEADER:]

    print("[" + datetime.now().strftime("%H:%M:%S.%f") + f" | CLIENT] String : {full_string}")

    reply = "[" + datetime.now().strftime("%H:%M:%S.%f") + " | SERVER] String input message received"
    conn.send(reply.encode(FORMAT))

    result = minPalPartion(full_string, 0, len(full_string) - 1)

    response = "[" + datetime.now().strftime("%H:%M:%S.%f") + f" | SERVER] Minimum cuts needed for palindrome partitioning is: {result}"
    conn.send(response.encode(FORMAT))

    # Receive konfirmasi dari client
    print(conn.recv(80).decode(FORMAT))

    conn.close()
    server.close()
    print("[" + datetime.now().strftime("%H:%M:%S.%f") + " | CLOSED] Server closed")
    return


def find_age(conn, server):
    # Receive message birthdate dari client
    message = conn.recv(HEADER).decode(FORMAT)
    
    print("[" + datetime.now().strftime("%H:%M:%S.%f") + f" | CLIENT] Message : {message}")

    reply = "[" + datetime.now().strftime("%H:%M:%S.%f") + " | SERVER] Birth date message received"
    conn.send(reply.encode(FORMAT))

    reformat = message.split('/')
    birthdate = dt.date(int(reformat[2]), int(reformat[1]), int(reformat[0]))
    today = dt.date.today()
    age = today.year - birthdate.year - ((today.month, today.day) < (birthdate.month, birthdate.day))

    response = "[" + datetime.now().strftime("%H:%M:%S.%f") + f" | SERVER] Result : {age} years old"
    conn.send(response.encode(FORMAT))

    # Receive konfirmasi dari client
    print(conn.recv(HEADER).decode(FORMAT))

    conn.close()
    server.close()
    print("[" + datetime.now().strftime("%H:%M:%S.%f") + " | CLOSED] Server closed")
    return


def main():
    while True:
        try:
            server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            print("[" + datetime.now().strftime("%H:%M:%S.%f") + " | SERVER] Socket created")
        except socket.error:
            print("[" + datetime.now().strftime("%H:%M:%S.%f") + " | SERVER] Socket creation failed %s" % socket.error)

        server.bind(ADDR)
        print("[" + datetime.now().strftime("%H:%M:%S.%f") + " | SERVER] Bind")

        server.listen()
        print("[" + datetime.now().strftime("%H:%M:%S.%f") + " | SERVER] Listening")

        conn, addr = server.accept()
        print("[" + datetime.now().strftime("%H:%M:%S.%f") + f" | SERVER] Connection from {addr} established")

        command = conn.recv(HEADER).decode(FORMAT)
        print("[" + datetime.now().strftime("%H:%M:%S.%f") + f" | CLIENT] Command : {command}")

        if int(command) == 1:
            # memberi tahu di worker node mana suatu job di eksekusi
            info = "[" + datetime.now().strftime("%H:%M:%S.%f") + f" | SERVER] Command dieksekusi oleh worker node di {socket.gethostname()}"
            conn.send(info.encode(FORMAT))
            find_age(conn, server)
        elif int(command) == 2:
            # memberi tahu di worker node mana suatu job di eksekusi
            info = "[" + datetime.now().strftime("%H:%M:%S.%f") + f" | SERVER] Command dieksekusi oleh worker node di {socket.gethostname()}"
            conn.send(info.encode(FORMAT))
            find_palpart(conn, server)
        else:
            server.close()
            print("[" + datetime.now().strftime("%H:%M:%S.%f") + " | CLOSED] Server closed")
            return


print("[" + datetime.now().strftime("%H:%M:%S.%f") + " | STARTS] Server is starting...")
main()
