import socket
import datetime
from datetime import datetime
import threading

HEADER = 80
FORMAT = 'utf-8'

PORT = 8080
SERVER = '54.172.198.52'
ADDR = (SERVER, PORT)


def get_palpart(client):
    string = ''
    string += str(input("[" + datetime.now().strftime("%H:%M:%S.%f") + " | CLIENT] Enter a string: "))
    string = f"{len(string):<{HEADER}}" + string
    client.send(string.encode(FORMAT))
    print("[" + datetime.now().strftime("%H:%M:%S.%f") + " | CLIENT] String input sent to server")

    # Receive konfirmasi dari server
    print(client.recv(80).decode(FORMAT))
    print("[" + datetime.now().strftime("%H:%M:%S.%f") + " | SERVER] Job is running...")
    
    # Receive hasil palindrome partitioning dari server
    print(client.recv(80).decode(FORMAT))

    reply = "[" + datetime.now().strftime("%H:%M:%S.%f") + " | CLIENT] Palindrome partition message received"
    client.send(reply.encode(FORMAT))
    print("[" + datetime.now().strftime("%H:%M:%S.%f") + " | SERVER] Job is finished")

    client.close()
    print("[" + datetime.now().strftime("%H:%M:%S.%f") + " | CLOSED] Client closed")
    return


def find_age(client):
    birthdate = ''
    birthdate += str(input("[" + datetime.now().strftime("%H:%M:%S.%f") + " | CLIENT] Enter your birth date (DD/MM/YYYY): "))
    birthdate = birthdate.encode('utf-8')
    client.send(birthdate)
    print("[" + datetime.now().strftime("%H:%M:%S.%f") + " | CLIENT] Birth date input sent to server")

    # Receive konfirmasi dari server
    print(client.recv(80).decode(FORMAT))
    print("[" + datetime.now().strftime("%H:%M:%S.%f") + " | SERVER] Job is running...")
    
    # Receive hasil kalkulasi umur dari server
    print(client.recv(80).decode(FORMAT))

    reply = "[" + datetime.now().strftime("%H:%M:%S.%f") + " | CLIENT] Age message received"
    client.send(reply.encode(FORMAT))
    print("[" + datetime.now().strftime("%H:%M:%S.%f") + " | SERVER] Job is finished")

    client.close()
    print("[" + datetime.now().strftime("%H:%M:%S.%f") + " | CLOSED] Client closed")
    return


def main():
    while True:
        try:
            client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            print("[" + datetime.now().strftime("%H:%M:%S.%f") + " | CLIENT] Socket created")
        except socket.error:
            print("[" + datetime.now().strftime("%H:%M:%S.%f") + " | CLIENT] Socket creation failed %s" % socket.error)

        client.connect(ADDR)
        print("[" + datetime.now().strftime("%H:%M:%S.%f") + " | CLIENT] Connected")

        command = str(input("[" + datetime.now().strftime("%H:%M:%S.%f") + " | CLIENT] Choose one of the following (1, 2 or 3):\n" 
                + "                           1 [Calculate Age]\n"
                + "                           2 [Palindrome Partitioning]\n"
                + "                           3 [Disconnect]\n"
                + "                           Your input: "))
        client.send(command.encode(FORMAT))
        print("[" + datetime.now().strftime("%H:%M:%S.%f") + " | CLIENT] Command input sent to server")

        # menerima informasi di worker mana job dijalankan
        print(client.recv(1024).decode(FORMAT))

        if int(command) == 1:
            find_age(client)
        elif int(command) == 2:
            get_palpart(client)
        else:
            client.close()
            print("[" + datetime.now().strftime("%H:%M:%S.%f") + " | CLOSED] Client closed")
            return
    

print("[" + datetime.now().strftime("%H:%M:%S.%f") + " | STARTS] Client is starting...")
main()
